﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyApps.Application.ViewModels
{
    public class SiteViewModel
    {
        public int IdSite { get; set; }
        public string NomSite { get; set; }
        public string AdresseSite { get; set; }  
    }
}
